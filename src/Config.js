const config_start_date = '18 Feb, 2019';
const config_end_date = '18 Mar, 2019';

const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];

const InternList = [
    "Phil",
    "Razan",
    "Patricia",
    "Abdullah",
    "Mustafa",
    "Ala'a",
    "Imad",
    "Adel",
    "Yazid" ,
    "Mireille",
    "Bassell"
];

module.exports = { 
    config_start_date,
    config_end_date,
    InternList,
    days
}
