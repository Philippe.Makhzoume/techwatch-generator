const _Config = require('./Config');
const _Calendar = require('./Calendar');
const _Utils = require('./Utils');
require('./SeedRandom');

const checkEquality = (internList, newInternList) => {
    const tmp = JSON.stringify(internList);
    const tmp2 = JSON.stringify(newInternList);
    const tmp2rev =  JSON.stringify(newInternList.reverse());

    if (tmp === tmp2 || tmp === tmp2rev)
        return true;
    
    return false;
}

// Should re-shuffle ? @todo use recursive here anyway
const createInternList = (internList, amountOfTurns, seeds) => {
    const ShuffledList = _Utils.shuffle(internList, seeds);
    const tmp = [];
    console.log(ShuffledList);
    while (amountOfTurns > 0) {
        ShuffledList.forEach(el => {
            tmp.push(el);
        });
        amountOfTurns--;
    }
    return tmp;
}

const main = (seedID = Math.random()) => {
    try {
        console.log("SeedOrigin", seedID);
        const randBySeed = new Math.seedrandom(seedID);
        const start_date = new Date(_Config.config_start_date);
        const end_date = new Date(_Config.config_end_date);

        const diff = _Calendar.getDayDiff(start_date, end_date);
        const Calendar = _Calendar.buildCalendar(diff, start_date);

        const internList = _Config.InternList;
        const amountOfTurns = _Utils.getAmountOfInternTurn(diff, internList.length);
        const list = createInternList(internList, amountOfTurns, randBySeed());

        if (checkEquality(internList, list))
            throw "Equality";

        const Final = [];
        Calendar.map((x, i) => {
            // Temporary > Move this to a config thing
            if (list[i] === "Imad" && (x.day === 1 || x.day === 4)) {
                // Redo until a valid array is found.
                throw "Invalid list";
            }

            Final.push({
                day: _Config.days[x.day],
                date: x.date,
                intern: list[i]
            })
        })

        console.log(randBySeed());
        console.log(Final);

        Final.forEach(x => {
            myFunction(`${x.day} - ${x.date}, ${x.intern}`);
        });
    } catch (e) {
        if (e === "Invalid list") {
            console.log("Invalid List Re-Seeding...", seedID)
            main(seedID);
        } else if (e === "Equality") {
            console.log("Equality Re-Seeding", seedID)
            main(seedID);
        }
        else
            console.log(e);
    }
}

// Temporary to edit HTML 
function myFunction(text) {
    var node = document.createElement("LI");
    var textnode = document.createTextNode(text);
    node.appendChild(textnode);
    document.getElementById("myList").appendChild(node);
}

main(6);