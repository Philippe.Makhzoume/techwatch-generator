const getDateFormat = (dateObject) => {
    try {
        const dayDate = dateObject.getDate();
        const dayMonth = dateObject.getMonth();
        const dayYear = dateObject.getFullYear();

        return `${dayDate}/${dayMonth}/${dayYear}`;
    } catch (e) {
        console.log(`getFullDateForThisDay Error : ${e}`);
    }
}

const getNextDayDate = (start, modifier) => {
    try {
        const day = new Date(start);
        const nextDay = new Date(day);
        nextDay.setDate(day.getDate() + modifier);
        return {
            date: getDateFormat(nextDay),
            day: nextDay.getDay()
        };
    } catch (e) {
        console.log(`GetNextDay Error : ${e}`)
    }
}

const getDayDiff = (start, end) => {
    try {
        const startGetDate = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
        const endGetDate = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());

        return Math.floor((endGetDate - startGetDate) / (1000 * 60 * 60 * 24));

    } catch (e) {
        console.log(`GetDaysDiff Error : ${e}`);
    }
}

const buildCalendar = (diff, start_date) => {
    try {
        const Calendar = [];
        // Should I check here if diff is <= 0 ?
        for (let i = 0; i <= diff; i++) {
            const nextDay = getNextDayDate(start_date, i);
            if (nextDay.day === 0 || nextDay.day === 6)
                continue;
            else
                Calendar.push(nextDay);
        }
        return Calendar;
    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    getDayDiff,
    buildCalendar
}