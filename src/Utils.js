let test = 0;
let temp = 0;
const shuffle = (array, seed) => {
    let m = array.length,
        t, i;

    // While there remain elements to shuffle…
    while (m) {
        // Pick a remaining element…
        i = Math.floor(seed * m--);
        temp++;
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }

    console.log("TestID : ", test++, "SEED :", seed, "EXPERIMENTS", temp);
    console.log("RESULT :", array);
    return array;
}

const getAmountOfInternTurn = (diff, listSize) => {
    return Math.floor(diff / listSize);
}

module.exports = {
    shuffle,
    getAmountOfInternTurn
}