# Techwatch Generator

Codi Techwatch Generator.
Build in Electron and JavaScript using part of Dayjs package.
It assign an intern to a specific day in a date range.

You can edit the config file in /src/config.js

- use : npm start
- ctrl+R : Reload
- ctrl+Shift+I : Toggle Inspector

# Task List
* [x] Config File
```
   > Start Date / End Date
   > Intern List
```
* [x] Calendar
```
    > Build a calendar based on Start / End Date
    > Skip Week-Ends
```
* [x] Utils
```
    > Shuffle the intern list (Fisher Yates)
    > Get the amount of turn an intern can do
```

* [x] Main
```
    > Update the U.I
    > Rescale the intern list based on the amount of turns
    > Assign Intern to a specific day.
    > Special Case handle an intern absence on specific days.
```

# Todo
* [ ] Cleanup src/Main.js mess + createInternList in recursive
* [ ] Better U.I
* [ ] Add More Config (i.e : Off days/Amount of Techwatch per day etc...)


# Optional Todo
* [ ] Turn this into functional programming